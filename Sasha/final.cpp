
#include "stdafx.h"
#include "math.h"
#include "conio.h"
#include "stdio.h"

typedef struct _PARTICLE
{
	double dCoordX; // координата частицы по х
	double dCoordY; // координата частицы по у
	double dSumPower; // суммарна€ сумма по двум частицам, которые Aндрей получает
	double dSumPowerX; // суммарна€ сумма по ’
	double dSumPowerY; // суммарна€ сумма по ”
	double dDX; // смещение по ’
	double dDY; // смещение по ”
} PARTICLE, *PPARTICLE;

#define PARTICLES_COUNT 10
#define PARTICLES_TIME	10
#define Pi				3.141592654;

double FSumPower()
{
	double dFSumPower = 0;

	return dFSumPower;
}

double GetR(PARTICLE ParticleArrayJ, PARTICLE ParticleArrayK)
{
	double dResult = 0;

	dResult = sqrt( (ParticleArrayJ.dCoordX - ParticleArrayK.dCoordX)*(ParticleArrayJ.dCoordX - ParticleArrayK.dCoordX) + (ParticleArrayJ.dCoordY - ParticleArrayK.dCoordY)*(ParticleArrayJ.dCoordY - ParticleArrayK.dCoordY) );

	return dResult;
}

double GetPowerCoordX(PARTICLE ParticleArrayJ, PARTICLE ParticleArrayK)
{
	double dFSumCoord = 0;

	dFSumCoord = ParticleArrayJ.dCoordX + ParticleArrayJ.dSumPower * ( ( ParticleArrayJ.dCoordX - ParticleArrayK.dCoordX) / GetR( ParticleArrayJ, ParticleArrayK ) );

	return dFSumCoord;
}

double GetPowerCoordY(PARTICLE ParticleArrayJ, PARTICLE ParticleArrayK)
{
	double dFSumCoord = 0;

	dFSumCoord = ParticleArrayJ.dCoordY + ParticleArrayJ.dSumPower * ( ( ParticleArrayJ.dCoordY - ParticleArrayK.dCoordY) / GetR( ParticleArrayJ, ParticleArrayK ) );

	return dFSumCoord;
}

int _tmain(int argc, _TCHAR* argv[])
{
	// начальные DX, DY, DT
	double dDX = 0;
	double dDY = 0;
	double dDT = 0;

	double dRO = 0;
	double dRP = 0;

	PARTICLE ParticleArray[PARTICLES_COUNT][PARTICLES_TIME];

	// цикл прохода по всем моментам времени
	for ( int nCountTime = 0; nCountTime < PARTICLES_TIME; nCountTime++ )
	{
		//цикл прохода по всем частицам в первый момент времени
		if ( nCountTime == 0 )
		{
			ParticleArray[0][nCountTime].dDX = dDX;
			ParticleArray[0][nCountTime].dDY = dDY;

			ParticleArray[0][nCountTime].dCoordX = ParticleArray[0][nCountTime].dDX;
			ParticleArray[0][nCountTime].dCoordY = ParticleArray[0][nCountTime].dDX;

			for ( int nCountParticle = 1; nCountParticle < PARTICLES_COUNT; nCountParticle++ )
			{
				ParticleArray[nCountParticle][nCountTime].dCoordX = ParticleArray[nCountParticle-1][nCountTime].dCoordX + dDX;
				ParticleArray[nCountParticle][nCountTime].dCoordY = ParticleArray[nCountParticle-1][nCountTime].dCoordY + dDY;
			}
		}
		else
		{
			for ( int nCountParticle = 0; nCountParticle < PARTICLES_COUNT; nCountParticle++ )
			{
				ParticleArray[nCountParticle][nCountTime].dSumPower = FSumPower(); // сюда надо подставить функцию вычислени€ суммарной суммы 

				for ( int nCountK = 0; nCountParticle < PARTICLES_COUNT; nCountParticle++ )
				{
					ParticleArray[nCountParticle][nCountTime].dSumPowerX += GetPowerCoordX(ParticleArray[nCountParticle][nCountTime], ParticleArray[nCountK][nCountTime]);
					ParticleArray[nCountParticle][nCountTime].dSumPowerY += GetPowerCoordY(ParticleArray[nCountParticle][nCountTime], ParticleArray[nCountK][nCountTime]);					
				}

				double dPi = Pi;
					
				double dMj = (4/3)*dRO*dRP*dPi*dRP*dRP;

				ParticleArray[nCountParticle][nCountTime].dDX = ( ParticleArray[nCountParticle][nCountTime].dSumPowerX * dDT * dDT ) / (2 * dMj);
				ParticleArray[nCountParticle][nCountTime].dDY = ( ParticleArray[nCountParticle][nCountTime].dSumPowerY * dDT * dDT ) / (2 * dMj);

				ParticleArray[nCountParticle][nCountTime].dCoordX = ParticleArray[nCountParticle][nCountTime-1].dCoordX + ParticleArray[nCountParticle][nCountTime].dDX;
				ParticleArray[nCountParticle][nCountTime].dCoordY = ParticleArray[nCountParticle][nCountTime-1].dCoordY + ParticleArray[nCountParticle][nCountTime].dDY;
			}
		}
	}

	FILE *FileToWrite;

	FileToWrite = fopen("particles.txt", "w");

	if ( FileToWrite )
	{
		int nNumberOfParticles = PARTICLES_COUNT;

		fprintf(FileToWrite, "Number of particles: %d \r\n", nNumberOfParticles);

		for ( int nTimes = 0; nTimes < PARTICLES_TIME; nTimes++ )
		{
			for ( int nParticle = 0; nParticle < PARTICLES_COUNT; nParticle++ )
			{
				fprintf(FileToWrite, "P %d: x=%f y=%f | ", nParticle, ParticleArray[nParticle][nTimes].dCoordX, ParticleArray[nParticle][nTimes].dCoordY);
			}

			fprintf(FileToWrite, "\r\n");
		}

		fclose(FileToWrite);
	}

	return 0;
}