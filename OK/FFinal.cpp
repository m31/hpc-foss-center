#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include "time.h"
#include "stdio.h"

using namespace std;

typedef struct _PARTICLE
{
	double dCoordX; 
	double dCoordY; 
	double dSumPower;
	double dSumPowerX;
	double dSumPowerY;
	double dDX;
	double dDY;
} PARTICLE, *PPARTICLE;



int Tclock=clock();

double rp;

const double e = 1.61e-19;
const double E0 = 8.85e-12;
const double pi = 3.141592654;
const double k = 1.38e-23;
const double h_ = 1.054e-34;
const double h = 6.626e-34;
const double me = 9.1e-31;
double T;
double w;
double np;
double nA;
double I;

double xx1;
double xx2;
double yy1;
double yy2;
const double eps = 0.0000001;

double cross_ne, cross_z, diff;


double Cc = 32; 
const double n0 = 4.0e+16;
double rD;
double rpi = rp;
double rpj = rp;

const double m=1.0;
const double D=1.0;

double phis = 1.0;
double phisj = 1.0; 
double phisi = 1.0; 

double Fdd;
double FSum;

double ne;
double R;

double dDX;
double dDY;
double dDT;

double dRO;
double dRP=rp;

double PARTICLES_COUNT=np;
double PARTICLES_TIME=100;

PARTICLE ParticleArray[PARTICLES_COUNT][PARTICLES_TIME];


ifstream in;
ofstream out;

double Rresearch(double inx1, double inx2, double iny1, double iny2)
{
	R = sqrt((inx1-inx2)*(inx1-inx2)+(iny1-iny2)*(iny1-iny2));
	return R;
}

vector<double> root2(double a, double b, double c)
{
    vector<double> result;
    double D=b*b-4*a*c;
    if (D<-eps) return result;
    if (D>eps)
    {
        D=sqrt(D);
        result.push_back((-b+D)/(2*a));
        result.push_back((-b-D)/(2*a));
        return result;
    }
    result.push_back(-b/(2*a));
    return result;
}


vector<double> root3(double a, double b, double c)
{
	vector<double> result;
	double Q=(a*a-3*b)/9;
	double R=(2*a*a*a-9*a*b+27*c)/54;
	double S=Q*Q*Q-R*R;
	double x1, x2, x3, phi;
	if (S>0)
	{
		phi=1.0/3*acos(R/pow(Q,1.5));
		x1=-2*sqrt(Q)*cos(phi)-a/3;
		x2=-2*sqrt(Q)*cos(phi+2.0/3*pi)-a/3;
		x3=-2*sqrt(Q)*cos(phi-2.0/3*pi)-a/3;
		result.push_back(x1);
		result.push_back(x2);
		result.push_back(x3);
		return result;
	}
	if(S<0)
	{
		if(Q<0)
		{
			phi=1.0/3*asinh(abs(R)/pow(abs(Q),1.5));
			x1=-2*(R?R/abs(R):R)*sqrt(Q)*sinh(phi)-a/3;
		}
		if(Q>0)
		{
			phi=1.0/3*acosh(abs(R)/pow(Q,1.5));
			x1=-2*(R?R/abs(R):R)*sqrt(Q)*cosh(phi)-a/3;
		}
		result.push_back(x1);
		return result;
	}
	x1=-2*(R?R/abs(R):R)*sqrt(Q)-a/3;
	x2=(R?R/abs(R):R)*sqrt(Q)-a/3;
	result.push_back(x1);
	if(x1!=x2) result.push_back(x2);
	return result;
}

double root_dichotomy(double (*f)(double x), double xMin, double xMax)
{
	double c, fc, fxMin, fxMax;
	while(abs(xMax-xMin)>2*eps)
	{

		c=(xMin+xMax)/2;
		fc=(*f)(c);
		if (abs(fc)<eps) break;
		fxMin=(*f)(xMin);
		fxMax=(*f)(xMax);
		if(fxMin==1E100 || fxMax==1E100) {c=1E100; break;}
		if (fxMax*fc>0) {xMax=c; continue;}
		if (fxMin*fc>0) {xMin=c; continue;}
		break;
	}
	return c;
}

double fz(double z)
{
	static double nue=2*pow(me*k*T/(2*pi*h_*h_),3.0/2);

	if(ne*ne-ne*z*np<0) return 1E100;
	double nq=sqrt(ne*ne-ne*z*np);

	double r0=sqrt(k*T/(8*pi*e*e*nq));
	double phis=(k*T*log(nue/nq)-w*e)/(3*e);
	double result=8*pi*E0*rp*(rp+r0)*k*T*sinh(e*phis/(2*k*T))/(e*e*r0)-z;
	return result;
}

double fz_nq_changes_ne(double z)
{
	static double nue=2*pow(me*k*T/(2*pi*h_*h_),3.0/2);
	double nq=ne;
	double r0=sqrt(k*T/(8*pi*e*e*nq));
	double phis=(k*T*log(nue/nq)-w*e)/(3*e);
	double result=8*pi*E0*rp*(rp+r0)*k*T*sinh(e*phis/(2*k*T))/(e*e*r0)-z;
	return result;
}

vector<double> fz2()
{
	static long double a=pow(2*pi*me*k*T/(h*h),3.0/2)*exp(-I*e/(k*T));
	vector<double> result=root3((a*a*np*np-3*np*np*ne*ne)/(np*np*np*ne),(3*np*ne*ne*ne+2*a*a*nA*np-2*a*a*np*ne)/(np*np*np*ne),-(ne*ne*ne*ne-a*a*ne*ne+2*a*a*nA*ne-a*a*nA*nA)/(np*np*np*ne));
	return result;
}

void find_solution(double Tval, double wval, double npval, double nAval, double rpval, double Ival)
{
	T=Tval;
	w=wval;
	np=npval;
	nA=nAval;
	rp=rpval;
	I=Ival;

	double nearr[5][10000];

	for(int kk=0; kk<5; kk++)
	  {	
	    for(int i=0; i<10000; i++)
	      {
		nearr[kk][i]=14+i*(26.0-14.0)/10000;
	      }
	  }
	cross_ne=0;
	cross_z=0;
	diff=10000000;
	for(int i=0; i<10000; i++)
	{
		ne=pow(10,nearr[0][i]);
	    nearr[3][i]=root_dichotomy(fz,-1E3,ne/np-eps);
	    nearr[4][i]=root_dichotomy(fz_nq_changes_ne,-1E3,1E3);
	    vector<double> result=fz2();
	    for(int i1=0; i1<result.size(); i1++)
	    {
	    	nearr[i1][i]=result[i1];
	        if(abs(nearr[i1][i]-nearr[4][i])<diff)
	        {
	        	diff=abs(nearr[i1][i]-nearr[4][i]);
	        	cross_ne=ne;
	        	cross_z=nearr[4][i];
	        }
	    }
	}
	rD=sqrt(k*T/(8*pi*e*e*ne));
	double nue=2*pow(me*k*T/(2*pi*h_*h_),3.0/2);
	phis=(k*T*log(nue/ne)-w*e)/(3*e);
	out<<"T="<<T<<"; "<<"w="<<w<<"; "<<"np="<<np<<"; "<<"nA="<<nA<<"; "<<"ne="<<cross_ne<<"; "<<"z="<<cross_z<<"; "<<"epsilon="<<diff<<"; "<<"rD="<<rD<<"; "<<"phis="<<phis<<"; "<<endl;
}

void FddResearch(double Rval)
{
	double R=Rval;

		Fdd = Cc*rp*rp*((exp(e*phis/(k*T))-1)/(sqrt(2*cosh( e*phis/(k*T))) )) * exp( rp/R*tanh(e*phis/(4*k*T)) );
		out<<"R="<<R<<"; "<<"Fdd="<<Fdd<<"; "<<endl;
}

double FSumResearch(double Rr)
{
  	double E=(-2*k*T*(rp/Rr) * (1/rD+1/Rr))/e*sinh(log(tanh(e*phis)))/((4*k*T)+copysign(1,phis)*(rp-Rr)/rD);
	double Fk = e*cross_z*E;
	double FSum = Fdd - Fk;
	out<<"FSum = "<<FSum<<endl;
}


double GetR(PARTICLE ParticleArrayJ, PARTICLE ParticleArrayK)
{
	double dResult = 0;

	dResult = sqrt( (ParticleArrayJ.dCoordX - ParticleArrayK.dCoordX)*(ParticleArrayJ.dCoordX - ParticleArrayK.dCoordX) + (ParticleArrayJ.dCoordY - ParticleArrayK.dCoordY)*(ParticleArrayJ.dCoordY - ParticleArrayK.dCoordY) );
	
	R = dResult;

	return dResult;
}

double GetPowerCoordX(PARTICLE ParticleArrayJ, PARTICLE ParticleArrayK)
{
	double dFSumCoord = 0;

	dFSumCoord = ParticleArrayJ.dCoordX + ParticleArrayJ.dSumPower * ( ( ParticleArrayJ.dCoordX - ParticleArrayK.dCoordX) / GetR( ParticleArrayJ, ParticleArrayK ) );

	return dFSumCoord;
}

double GetPowerCoordY(PARTICLE ParticleArrayJ, PARTICLE ParticleArrayK)
{
	double dFSumCoord = 0;

	dFSumCoord = ParticleArrayJ.dCoordY + ParticleArrayJ.dSumPower * ( ( ParticleArrayJ.dCoordY - ParticleArrayK.dCoordY) / GetR( ParticleArrayJ, ParticleArrayK ) );

	return dFSumCoord;
}

double Ffinal ()
{

	
#pragma omp parallel
{
#pragma omp for
	for ( int nCountTime = 0; nCountTime < PARTICLES_TIME; nCountTime++ )
	{
		if ( nCountTime == 0 )
		{
			ParticleArray[0][nCountTime].dDX = dDX;
			ParticleArray[0][nCountTime].dDY = dDY;

			ParticleArray[0][nCountTime].dCoordX = ParticleArray[0][nCountTime].dDX;
			ParticleArray[0][nCountTime].dCoordY = ParticleArray[0][nCountTime].dDX;

			for ( int nCountParticle = 1; nCountParticle < PARTICLES_COUNT; nCountParticle++ )
			{
				ParticleArray[nCountParticle][nCountTime].dCoordX = ParticleArray[nCountParticle-1][nCountTime].dCoordX + dDX;
				ParticleArray[nCountParticle][nCountTime].dCoordY = ParticleArray[nCountParticle-1][nCountTime].dCoordY + dDY;
			}
		}
		else
		{
			for ( int nCountParticle = 0; nCountParticle < PARTICLES_COUNT; nCountParticle++ )
			{
				ParticleArray[nCountParticle][nCountTime].dSumPower = FSum;

				for ( int nCountK = 0; nCountParticle < PARTICLES_COUNT; nCountParticle++ )
				{
					ParticleArray[nCountParticle][nCountTime].dSumPowerX += GetPowerCoordX(ParticleArray[nCountParticle][nCountTime], ParticleArray[nCountK][nCountTime]);
					ParticleArray[nCountParticle][nCountTime].dSumPowerY += GetPowerCoordY(ParticleArray[nCountParticle][nCountTime], ParticleArray[nCountK][nCountTime]);					
				}

				double dpi = pi;
					
				double dMj = (4/3)*dRO*dRP*dpi*dRP*dRP;

				ParticleArray[nCountParticle][nCountTime].dDX = ( ParticleArray[nCountParticle][nCountTime].dSumPowerX * dDT * dDT ) / (2 * dMj);
				ParticleArray[nCountParticle][nCountTime].dDY = ( ParticleArray[nCountParticle][nCountTime].dSumPowerY * dDT * dDT ) / (2 * dMj);

				ParticleArray[nCountParticle][nCountTime].dCoordX = ParticleArray[nCountParticle][nCountTime-1].dCoordX + ParticleArray[nCountParticle][nCountTime].dDX;
				ParticleArray[nCountParticle][nCountTime].dCoordY = ParticleArray[nCountParticle][nCountTime-1].dCoordY + ParticleArray[nCountParticle][nCountTime].dDY;
			}
		}
	}
}

	FILE *FileToWrite;

	FileToWrite = fopen("outFFinal.txt", "w");

	if ( FileToWrite )
	{
		int nNumberOfParticles = PARTICLES_COUNT;

		fprintf(FileToWrite, "Number of particles: %d \r\n", nNumberOfParticles);

		for ( int nTimes = 0; nTimes < PARTICLES_TIME; nTimes++ )
		{
			for ( int nParticle = 0; nParticle < PARTICLES_COUNT; nParticle++ )
			{
				fprintf(FileToWrite, "P %d: x=%f y=%f | ", nParticle, ParticleArray[nParticle][nTimes].dCoordX, ParticleArray[nParticle][nTimes].dCoordY);
			}

			fprintf(FileToWrite, "\r\n");
		}

		fclose(FileToWrite);
	}

}

int main()
{
	in.open("inFFinal.txt",ios::in);
	while(in>>T>>w>>np>>nA>>rp>>I>>dDX>>dDY>>dDT>>dRO)
	{
		find_solution(T,w,np,nA,rp,I);
		FddResearch(R);
		FSumResearch(R);
		Ffinal();
	}
	cout<<double(clock()-Tclock) / CLOCKS_PER_SEC<<endl;
	return 0;
}
