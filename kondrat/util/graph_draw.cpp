#include "func.h"
#include <iostream>
#include <cmath>
//#include <mgl2/mgl.h>
#include <mgl/mgl_zb.h>

using namespace std;

const double rp  = 1.0e-7; // rp=10^(-7)

const double e = 1.61e-19;
const double E0 = 8.85e-12;
const double pi = 3.141592654;
const double k = 1.38e-23;
const double h_ = 1.054e-34;
const double h = 6.626e-34;
const double me = 9.1e-31;
const double T = 2400;
const double w = 2.5;
const double np = 1.0e+13;
const double na = 1.0e+15;
const double I = 3.893;
const double eps = 0.0000001;

double ne=1E19;


// Функция поиска решения трансцендентного уравнения(для нахождения решения необходимо передавать её в дихотомию)
double fz(double z)
{
	static double nue=2*pow(me*k*T/(2*pi*h_*h_),3.0/2);
	//cout<<nue<<endl;
	//cout<<(ne*ne-ne*z*np)<<endl;
	if(ne*ne-ne*z*np<0) return 1E100;
	double nq=sqrt(ne*ne-ne*z*np);
	//cout<<nq<<endl;
	double r0=sqrt(k*T/(8*pi*e*e*nq));
	//cout<<r0<<endl;
	double phis=(k*T*log(nue/nq)-w*e)/(3*e);
	//cout<<phis<<endl;
	double result=8*pi*E0*rp*(rp+r0)*k*T*sinh(e*phis/(2*k*T))/(e*e*r0)-z;
	return result;
}

//Функция поиска решений кубического уравнения относительно z
vector<double> fz2()
{
	static long double a=pow(2*pi*me*k*T/(h*h),3.0/2)*exp(-I*e/(k*T));
	vector<double> result=root3((a*a*np*np-3*np*np*ne*ne)/(np*np*np*ne),(3*np*ne*ne*ne+2*a*a*na*np-2*a*a*np*ne)/(np*np*np*ne),-(ne*ne*ne*ne-a*a*ne*ne+2*a*a*na*ne-a*a*na*na)/(np*np*np*ne));
	return result;
}

// Функция отрисовки кривых решений обоих уравнений
void draw(mglGraph *gr, void *)
    {
        gr->NewFrame();
        mglData y(1000,4);
        y.Fill("x",mglPoint(14),mglPoint(26));
        //y.Fill("x",mglPoint(19),mglPoint(21));

        double cross_ne=0, cross_z=0, diff=10000000;

        for(int i=0; i<1000; i++)
        {
        	ne=pow(10,y.a[i]);
        	for(int i1=0; i1<4; i1++) y.a[i+1000*i1]=1E100;
        	y.a[i+1000*3]=root_dichotomy(fz,-1E2,1E2);
        	vector<double> result=fz2();
        	for(int i1=0; i1<result.size(); i1++)
        	{
        		y.a[i+1000*i1]=result[i1];
        		if(abs(y.a[i+1000*i1]-y.a[i+1000*3])<diff) //cout<<ne<<" "<<y.a[i+1000*3]<<endl;
        		{
        			diff=abs(y.a[i+1000*i1]-y.a[i+1000*3]);
        			cross_ne=ne;
        			cross_z=y.a[i+1000*3];
        		}
        	}
        }
        cout<<cross_ne<<" "<<cross_z<<" "<<diff<<endl;

        /*for(int i=0; i<1000; i++)
        {
            for(int i1=0; i1<4; i1++) if(y.a[i+1000*i1]<-1E2 || y.a[i+1000*i1]>1E2) y.a[i+1000*i1]=1E100;
        }
        for(int i=0; i<999; i++)
        {
        	for(int i1=0; i1<4; i1++) if(abs(y.a[i+1000*i1]-y.a[i+1+1000*i1])>50) y.a[i+1000*i1]=1E100;
        }*/
        gr->Axis("xy",true);
        gr->Plot(y);
        gr->EndFrame();
    }

void draw_ffz_z(mglGraph *gr, void *)
    {
        gr->NewFrame();
        mglData y(1000,1);
        y.Fill("x",mglPoint(-1000),mglPoint(1000));
        //y.Fill("x",mglPoint(19),mglPoint(21));

        for(int i=0; i<1000; i++)
        {
        	y.a[i]=fz(y.a[i]);
        }

        gr->Axis("xy",true);
        gr->Plot(y);
        gr->EndFrame();
    }

int main()
{
	mglGraphZB gr;
	gr.SetRanges(14,26,-1000,1000,0,0);
	gr.SetAlphaDef(1);
	draw(&gr,NULL);           // The same drawing function.
	gr.WritePNG("test.png");    // Don't forget to save the result!

	/*ne=1E20;
	mglGraphZB gr;
	gr.SetRanges(-1000,1000,-1000,1000,0,0);
	draw_ffz_z(&gr,NULL);           // The same drawing function.
	gr.WritePNG("test.png");    // Don't forget to save the result!*/

	//ne=5E+17;

	/*
	for (int deg=15; deg<22; deg++)
	{
		ne=pow(10.0,double(deg));
		cout<<"ne="<<ne<<endl;
		vector<double> result=fz2();
		for(int i=0; i<result.size(); i++)
		{
			cout<<result[i]<<" ";
		}
		cout<<endl;
		cout<<root_dichotomy(fz,-2E2,100)<<endl<<endl;
	}*/

	ne=1e+16;

	//cout<<root_dichotomy(fz,-100,40)<<endl<<endl;

	//cout<<fz(35.0)<<endl;

	vector<double> result=fz2();
	for(int i=0; i<result.size(); i++)
	{
		cout<<result[i]<<" ";
	}

	cout<<endl;
	/*cout<<root_dichotomy(fz,-1E2,1E2)<<endl;*/
	return 0;
}
