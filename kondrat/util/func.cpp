#include "func.h"
#include <cmath>
#include <iostream>

#define eps 0.0000001
#define pi 3.141592654

using namespace std;

//bool err_flag=0;

vector<double> root2(double a, double b, double c)
{
    vector<double> result;
    double D=b*b-4*a*c;
    if (D<-eps) return result;
    if (D>eps)
    {
        D=sqrt(D);
        result.push_back((-b+D)/(2*a));
        result.push_back((-b-D)/(2*a));
        return result;
    }
    result.push_back(-b/(2*a));
    return result;
}

/*void root_dichotomy(double (*f)(double x), double xMin, double xMax, vector<double> result)
{
    double c, fc;
    while(/*а нужен ли здесь модуль?*/ /*abs(xMax-xMin)>2*eps)
   {
        c=(xMin+xMax)/2;
        fc=(*f)(c);
        if (abs(fc)<eps) {root((*f),xMin,c,result); root((*f),c,xMax,result); break;}
        if ((*f)(xMin)*fc<0) {xMax=c; continue;}
        if ((*f)(xMax)*fc>0) {xMin=c; continue;}
        break;
    }
    result.push_back(c);
    return result;
}*/


vector<double> root3(double a, double b, double c)
{
	vector<double> result;
	double Q=(a*a-3*b)/9;
	double R=(2*a*a*a-9*a*b+27*c)/54;
	double S=Q*Q*Q-R*R;
	double x1, x2, x3, phi;
	if (S>0)
	{
		phi=1.0/3*acos(R/pow(Q,1.5));
		x1=-2*sqrt(Q)*cos(phi)-a/3;
		x2=-2*sqrt(Q)*cos(phi+2.0/3*pi)-a/3;
		x3=-2*sqrt(Q)*cos(phi-2.0/3*pi)-a/3;
		result.push_back(x1);
		result.push_back(x2);
		result.push_back(x3);
		return result;
	}
	if(S<0)
	{
		if(Q<0)
		{
			phi=1.0/3*asinh(abs(R)/pow(abs(Q),1.5));
			x1=-2*(R?R/abs(R):R)*sqrt(Q)*sinh(phi)-a/3;
		}
		if(Q>0)
		{
			phi=1.0/3*acosh(abs(R)/pow(Q,1.5));
			x1=-2*(R?R/abs(R):R)*sqrt(Q)*cosh(phi)-a/3;
		}
		result.push_back(x1);
		return result;
	}
	x1=-2*(R?R/abs(R):R)*sqrt(Q)-a/3;
	x2=(R?R/abs(R):R)*sqrt(Q)-a/3;
	result.push_back(x1);
	if(x1!=x2) result.push_back(x2);
	return result;
}

double root_dichotomy(double (*f)(double x), double xMin, double xMax)
{
	double c, fc, fxMin, fxMax;
	while(/*а нужен ли здесь модуль?*/abs(xMax-xMin)>2*eps)
	{

		c=(xMin+xMax)/2;
		fc=(*f)(c);
		if (abs(fc)<eps) break;
		fxMin=(*f)(xMin);
		fxMax=(*f)(xMax);
		if(fxMin==1E100 || fxMax==1E100) {c=1E100; break;}
		//if (err_flag) {err_flag=0; return 1E50;}
		//if(abs(log10(fxMin))>4) cout<<fxMin<<endl;
		if (fxMax*fc>0) {xMax=c; continue;}
		if (fxMin*fc>0) {xMin=c; continue;}
		break;
	}
	return c;
}


// НЕ РАБОТАЕТ
/*double root_iter(double (*f)(double x), double xApprox)
{
	static int deep=0;
	double fx;
	if (xApprox!=0) fx=(*f)(xApprox);
	if (xApprox!=0 && abs(fx-xApprox)>eps && deep<1000) {deep++; return root_iter(f, fx);}
	deep=0;
	return xApprox;
}
*/
