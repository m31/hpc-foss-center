#include "func.h"
#include <iostream>
#include <cmath>

using namespace std;

const double rp  = 0.000001; // радиус частицы - 1 мкм

double f(double rD)
{
    return rD*rD*rD-9*rD*rD+20*rD-11;//rD*rD-rp*(rp+rD)*exp(rp/rD);
}

int main()
{
    double a,b,c,d,rDMin,rDMax;
    while(!cin.eof())
    {
       cout<<"Enter coefficients of square equation(a b c): ";
        cin>>a>>b>>c;
        vector<double> answer=root2(a,b,c);
        for(int i=0; i<answer.size(); i++) cout<<answer[i]<<endl;
        //cout<<"Enter coefficients of cubic equation(a b c d): ";
        cout<<"Enter coefficients of transcendental equation(rDMin rDMax): ";
        cin>>rDMin>>rDMax;
        cout<<root(f,rDMin,rDMax)<<endl;
    }
    return 0;
}
