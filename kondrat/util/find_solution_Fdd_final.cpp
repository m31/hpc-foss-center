#include "func.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include "time.h"

using namespace std;

const double rp  = 1.0e-7; // rp=10^(-7)

const double e = 1.61e-19;
const double E0 = 8.85e-12;
const double pi = 3.141592654;
const double k = 1.38e-23;
const double h_ = 1.054e-34;
const double h = 6.626e-34;
const double me = 9.1e-31;
double T = 2400;
double w = 2.5;
double np = 1.0e+15;
double na = 1.0e+16;
const double I = 3.893;
const double eps = 0.0000001;

double cross_ne, cross_z, diff;


const double nA = 2e+23;
	double Cc = 32; // C для проверочного построения
	const double n0 = 4.0e+16;
	double rD = 4.0e-7;
	//const double rp = 1.0e-8;
	double rpi = rp;
	double rpj = rp;
	//const double np = 1.0e+18;

	const double m=1.0;
	const double D=1.0;

	double phis = 1.0;
	double phisj = 1.0; // =Fis
	double phisi = 1.0; // =Fis

	//double  R;// [10^5, 10^8]
	double Fdd[15];

double ne;

ifstream in;
ofstream out;

// Функция поиска решения трансцендентного уравнения(для нахождения решения необходимо передавать её в дихотомию)
double fz(double z)
{
	static double nue=2*pow(me*k*T/(2*pi*h_*h_),3.0/2);
	//cout<<nue<<endl;
	//cout<<(ne*ne-ne*z*np)<<endl;

	if(ne*ne-ne*z*np<0) return 1E100;
	double nq=sqrt(ne*ne-ne*z*np);

	//cout<<nq<<endl;
	double r0=sqrt(k*T/(8*pi*e*e*nq));
	//cout<<r0<<endl;
	double phis=(k*T*log(nue/nq)-w*e)/(3*e);
	//cout<<phis<<endl;
	double result=8*pi*E0*rp*(rp+r0)*k*T*sinh(e*phis/(2*k*T))/(e*e*r0)-z;
	return result;
}

// Аналог предыдущей, но nq заменено на ne
double fz_nq_changes_ne(double z)
{
	static double nue=2*pow(me*k*T/(2*pi*h_*h_),3.0/2);
	//cout<<nue<<endl;
	//cout<<(ne*ne-ne*z*np)<<endl;

	//if(ne*ne-ne*z*np<0) return 1E100;
	//double nq=sqrt(ne*ne-ne*z*np);
	double nq=ne;
	//cout<<nq<<endl;
	double r0=sqrt(k*T/(8*pi*e*e*nq));
	//cout<<r0<<endl;
	double phis=(k*T*log(nue/nq)-w*e)/(3*e);
	//cout<<phis<<endl;
	double result=8*pi*E0*rp*(rp+r0)*k*T*sinh(e*phis/(2*k*T))/(e*e*r0)-z;
	return result;
}

//Функция поиска решений кубического уравнения относительно z
vector<double> fz2()
{
	static long double a=pow(2*pi*me*k*T/(h*h),3.0/2)*exp(-I*e/(k*T));
	vector<double> result=root3((a*a*np*np-3*np*np*ne*ne)/(np*np*np*ne),(3*np*ne*ne*ne+2*a*a*na*np-2*a*a*np*ne)/(np*np*np*ne),-(ne*ne*ne*ne-a*a*ne*ne+2*a*a*na*ne-a*a*na*na)/(np*np*np*ne));
	return result;
}

void find_solution(double Tval, double wval, double npval, double naval)
{
	T=Tval;
	w=wval;
	np=npval;
	na=naval;

	double nearr[5][10000];

	for(int k=0; k<5; k++)
		for(int i=0; i<10000; i++) nearr[k][i]=14+i*(26.0-14.0)/10000;

	cross_ne=0;
	cross_z=0;
	diff=10000000;

	for(int i=0; i<10000; i++)
	{
		ne=pow(10,nearr[0][i]);
	    nearr[3][i]=root_dichotomy(fz,-1E3,ne/np-eps);
	    nearr[4][i]=root_dichotomy(fz_nq_changes_ne,-1E3,1E3);
	    vector<double> result=fz2();
	    for(int i1=0; i1<result.size(); i1++)
	    {
	    	nearr[i1][i]=result[i1];
	        /*if(abs(nearr[i1][i]-nearr[3][i])<diff) //cout<<ne<<" "<<y.a[i+1000*3]<<endl;
	        {
	        	diff=abs(nearr[i1][i]-nearr[3][i]);
	        	cross_ne=ne;
	        	cross_z=nearr[3][i];
	        }*/
	        if(abs(nearr[i1][i]-nearr[4][i])<diff) //cout<<ne<<" "<<y.a[i+1000*3]<<endl;
	        {
	        	diff=abs(nearr[i1][i]-nearr[4][i]);
	        	cross_ne=ne;
	        	cross_z=nearr[4][i];
	        }
	    }
	}
	rD=sqrt(k*T/(8*pi*e*e*ne));
	double nue=2*pow(me*k*T/(2*pi*h_*h_),3.0/2);
	phis=(k*T*log(nue/ne)-w*e)/(3*e);
	out<<"T="<<T<<"; "<<"w="<<w<<"; "<<"np="<<np<<"; "<<"na="<<na<<"; "<<"ne="<<cross_ne<<"; "<<"z="<<cross_z<<"; "<<"epsilon="<<diff<<"; "<<"rD="<<rD<<"; "<<"phis="<<phis<<"; "<<endl;
}

void FddResearch()
{
	double R=4*rD;
	for (int i = 0; R<=10*rD; R+=rD, i++)
	{
		Fdd[i] = Cc*rp*rp*((exp(e*phis/(k*T))-1)/(sqrt(2*cosh( e*phis/(k*T))) )) * exp( rp/R*tanh(e*phis/(4*k*T)) );
		out<<"R="<<R<<"; "<<"Fdd="<<Fdd[i]<<"; "<<endl;
	}
}

int main()
{
	//int t=clock();
	in.open("in.txt",ios::in);
	out.open("out.txt",ios::out);
	while(in>>T>>w>>np>>na)
	{
		find_solution(T,w,np,na);
		FddResearch();
	}
	//cout<<double(clock()-t) / CLOCKS_PER_SEC<<endl;
	return 0;
}
