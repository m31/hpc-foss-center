#include <vector>
#include <cmath>

using namespace std;

// функция взятия корней квадратного уравнения вида ax^2 + bx + c = 0
vector<double> root2(double a, double b, double c);

// Функция взятия корней кубического уравнения вида x^3 + ax^2 + bx + c = 0
vector<double> root3(double a, double b, double c);

// функция получения корня трансцендентного уравнения вида f(x)=0 методом дихотомии
double root_dichotomy(double (*f)(double x), double xMin, double xMax);

// функция получения корня трансцендентного уравнения вида x=f(x) методом итераций НЕ РАБОТАЕТ
//double root_iter(double (*f)(double x), double xApprox);
