/*
 * find_z_ne_relation.cpp
 *
 *  Created on: 18 апр. 2013
 *      Author: kondrat
 */

#include <iostream>
#include <string>
#include <cmath>
#include "func.h"

using namespace std;

const double I = 3.893;
//const double W = 3.7;
const double W0 = 3.7;
const double rp = 1E-8;
const double np = 1E16;
const double K = 1.38E-23;
const double h = 6.62E-34;
const double h_ = 1.054e-34;
const double me = 9.1E-31;
const double e = 1.6E-19;
const double ge = 2;
const double gi = 0.5;
const double ga = 1;
const double na = 1E17;
const double T = 2700;

double ks;
double A;
double nue;

struct PlasmaParameters
{
	double ni;
	double ne;
	double z;
	double phipl;
};

// Функция поиска решения трансцендентного уравнения(для нахождения решения необходимо передавать её в дихотомию)
double fni(double ni)
{
	return (na-ni)*(na-ni)/(ni*ni*ni)*(ks*exp(-I/(K*T)))*(ks*exp(-I/(K*T))) -
			ni -
			np*(rp*K*T/(e*e)*(0.5*(log(ni)+log( (na-ni)*(na-ni)/(ni*ni*ni)*(ks*exp(-I/(K*T)))*(ks*exp(-I/(K*T))) )) +
					log(e*nue/(4*A*T*T)) + W0/(K*T)));
}

PlasmaParameters* find_solution(double min_ni, double max_ni)
{
	PlasmaParameters* param = new PlasmaParameters();
	param->ni=root_dichotomy(fni,min_ni,max_ni);
	double ni=param->ni;
	param->ne=(na-ni)*(na-ni)/(ni*ni*ni)*(ks*exp(-I/(K*T)))*(ks*exp(-I/(K*T)));
	param->z=(rp*K*T/(e*e)*(0.5*(log(ni)+log( (na-ni)*(na-ni)/(ni*ni*ni)*(ks*exp(-I/(K*T)))*(ks*exp(-I/(K*T))) )) +
			log(e*nue/(4*A*T*T)) + W0/(K*T)));
	param->phipl=K*T/(2*e)*log(param->ni/param->ne);
	return param;
}

void print_parameters(PlasmaParameters* param)
{
	cout<<"ni="<<param->ni<<"; ne="<<param->ne<<"; z="<<param->z<<"; phipl="<<param->phipl<<endl;
}

int main()
{
	// raschet konstanl
	ks =  (ge * gi / ga ) * pow((( 2.0 * M_PI * me * K * T) / (h * h)), 1.5) ;
	nue=2*pow(me*K*T/(2*M_PI*h_*h_),3.0/2);
	A=4*M_PI*K*K*me/(h*h);

	PlasmaParameters* param=find_solution(1E17,1E19);

	for(double i=1; i<1E30; i*=10) cout<<fni(i)<<" ";

	//print_parameters(param);

	return 0;
}


