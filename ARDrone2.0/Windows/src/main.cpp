#include "ardrone/ardrone.h"

#define KEY_DOWN(key) (GetAsyncKeyState(key) & 0x8000)
#define KEY_PUSH(key) (GetAsyncKeyState(key) & 0x0001)



CvMemStorage* storage = NULL;
// AR.Drone class
    ARDrone ardrone;

 IplImage *image = 0, * imgRedThresh=0, *imgContours=0;

 bool hand_control=true;

double pos_x=0, pos_y=0, radius=0;

void left()
{
    double x = 0.0, y = 0.0, z = 0.0, r = 0.0;
   printf("%s\n","left");
   if (!ardrone.onGround() && !hand_control) {
      r = 0.5;
      ardrone.move3D(x, y, z, r);
   }
}

void right()
{
    double x = 0.0, y = 0.0, z = 0.0, r = 0.0;
    printf("%s\n","right");
   if (!ardrone.onGround() && !hand_control) {
      r = -0.5;
      ardrone.move3D(x, y, z, r);
   }
}

void front()
{
    double x = 0.0, y = 0.0, z = 0.0, r = 0.0;
    printf("%s\n","front");
   if (!ardrone.onGround() && !hand_control) {
      x = 0.5;
      ardrone.move3D(x, y, z, r);
   }
}

void back()
{
    double x = 0.0, y = 0.0, z = 0.0, r = 0.0;
    printf("%s\n","back");
   if (!ardrone.onGround() && !hand_control) {
      x = -0.5;
      ardrone.move3D(x, y, z, r);
   }
}

IplImage* GetThresholdedImage(IplImage* img)
{
	// Convert the image into an HSV image
	IplImage* imgHSV = cvCreateImage(cvGetSize(img), 8, 3);
	cvCvtColor(img, imgHSV, CV_BGR2HSV);

	IplImage* imgThreshed = cvCreateImage(cvGetSize(img), 8, 1);
   

	// Values 20,100,100 to 30,255,255 working perfect for Red at around 6pm
	cvInRangeS(imgHSV, cvScalar(158, 100, 100), cvScalar(180, 255, 255), imgThreshed);

	cvReleaseImage(&imgHSV);

	return imgThreshed;
}

IplImage* GetContoursImage(IplImage* thresh_img)
{
   //IplImage* contours_img=cvCloneImage(thresh_img);
   if( storage==NULL ) {
        storage = cvCreateMemStorage(0);
    } else {
        cvClearMemStorage( storage );
    }
   IplImage* contours_img=cvCreateImage(cvGetSize(thresh_img),8,1);
   CvSeq* contours=0;
   cvFindContours(thresh_img,storage,&contours);
   double max_area=0;
   CvSeq* max_area_contour=0;
   while(contours)
   {
      if(cvContourArea(contours)>100) cvDrawContours(
         contours_img,
         contours,
         cvScalarAll(255),
         cvScalarAll(255),
         0,-1
      );
      if(cvContourArea(contours)>max_area)
      {
         max_area=cvContourArea(contours);
         max_area_contour=contours;
      }
      contours=(*contours).h_next;
   }
   if(max_area_contour){
   /*IplImage* mask=cvCreateImage(cvGetSize(thresh_img),8,1);;
   cvDrawContours(
         mask,
         max_area_contour,
         cvScalarAll(255),
         cvScalarAll(255),
         0,-1
      );
   for(int x=0; x<mask->width; x++)
			for(int y=0; y<mask->height; y++)
			{
				CV_IMAGE_ELEM( thresh_img, uchar, y, x)=CV_IMAGE_ELEM( mask, uchar, y, x);//*=(CV_IMAGE_ELEM( mask, uchar, y, x)!=0?1:0);
			}*/
   CvPoint2D32f center;
   float rad;
   cvMinEnclosingCircle(max_area_contour,&center,&rad);
   pos_x=center.x;
   pos_y=center.y;
   radius=rad;
   cvCircle(thresh_img,cvPoint(pos_x,pos_y),radius,cvScalar(255));
   cvDrawContours(
         thresh_img,
         max_area_contour,
         cvScalarAll(255),
         cvScalarAll(255),
         0,-1
      );

   if(radius>100) back();
   else if(radius<30) front();
   else
   {
      ardrone.move3D(0, 0, 0, 0);
   }
   }
   cvReleaseMemStorage(&storage);
   return contours_img;
}





// --------------------------------------------------------------------------
// main(Number of arguments, Value of arguments)
// This is the main function.
// Return value Success:0 Error:-1
// --------------------------------------------------------------------------
int main(int argc, char **argv)
{
    

    // Initialize
    if (!ardrone.open()) {
        printf("Failed to initialize.\n");
        return -1;
    }
    IplImage* imgScribble = NULL;
    // Main loop
    while (!GetAsyncKeyState(VK_ESCAPE)) {
        // Update your AR.Drone
        if (!ardrone.update()) break;

        // Get an image
        image = ardrone.getImage();

        ///////////////////////////////////////////////////////////////////////////////////////////////////

        // Will hold a frame captured from the camera
		//IplImage* frame = 0;
		//frame = cvQueryFrame(capture);

		// If we couldn't grab a frame... quit
        if(!image)
            break;
		
		// If this is the first frame, we need to initialize it
		if(imgScribble == NULL)
		{
			imgScribble = cvCreateImage(cvGetSize(image), 8, 3);
		}

		// Holds the Red thresholded image (Red = white, rest = black)
		imgRedThresh = GetThresholdedImage(image);

      imgContours = GetContoursImage(imgRedThresh);
      cvShowImage("thresh", imgRedThresh);
      cvSub(imgRedThresh,imgContours,imgRedThresh);

		// Calculate the moments to estimate the position of the ball
		/*CvMoments *moments = (CvMoments*)malloc(sizeof(CvMoments));
		cvMoments(imgRedThresh, moments, 1);

		// The actual moment values
		double moment10 = cvGetSpatialMoment(moments, 1, 0);
		double moment01 = cvGetSpatialMoment(moments, 0, 1);
		double area = cvGetCentralMoment(moments, 0, 0);

		// Holding the last and current ball positions
		static int posX = 0;
		static int posY = 0;

		int lastX = posX;
		int lastY = posY;

		posX = moment10/area;
		posY = moment01/area;
      */
		// Print it out for debugging purposes
		//printf("position (%d,%d)\n", posX, posY);

      cvZero(imgScribble);

		// We want to draw a line only if its a valid position
		/*if(lastX>0 && lastY>0 && posX>0 && posY>0)
		{
			// Draw a Red line from the previous point to the current point
			cvLine(imgScribble, cvPoint(posX, posY), cvPoint(lastX, lastY), cvScalar(0,255,255), 5);
		}*/
      \
      if(/*lastX>0 && lastY>0 && posX>0 && posY>0*/pos_x>0 && pos_x>0)
		{
			// Draw a Red line from the previous point to the current point
			cvLine(imgScribble, cvPoint(pos_x, pos_y), cvPoint(pos_x, pos_y), cvScalar(0,255,255), 5);
		}

      
      if(abs(pos_x-(*image).width/2)>150)
      {
         if(pos_x>(*image).width/2) right();
         else left();
      }
      else
      {
         //r = 0.0;
         ardrone.move3D(0, 0, 0, 0);
      }

		// Add the scribbling image and the frame... and we get a combination of the two
		cvAdd(image, imgScribble, image);
		//cvShowImage("thresh", imgRedThresh);
		cvShowImage("video", image);
		cvShowImage("contours", imgContours);


		// Wait for a keypress
		int c = cvWaitKey(33);
		/*if(c!=-1)
		{
			// If pressed, break out of the loop
            break;
		}*/

		// Release the thresholded image... we need no memory leaks.. please
		cvReleaseImage(&imgRedThresh);

		//delete moments;


      ///////////////////////////////////////////////////////////////////////////////////////////////

        // Orientation
        double roll  = ardrone.getRoll();
        double pitch = ardrone.getPitch();
        double yaw   = ardrone.getYaw();
        //printf("ardrone.roll  = %3.2f [deg]\n", roll  * RAD_TO_DEG);
        //printf("ardrone.pitch = %3.2f [deg]\n", pitch * RAD_TO_DEG);
        //printf("ardrone.yaw   = %3.2f [deg]\n", yaw   * RAD_TO_DEG);

        // Altitude
        double altitude = ardrone.getAltitude();
       // printf("ardrone.altitude = %3.2f [m]\n", altitude);

        // Velocity
        double vx, vy, vz;
        double velocity = ardrone.getVelocity(&vx, &vy, &vz);
        //printf("ardrone.vx = %3.2f [m/s]\n", vx);
        //printf("ardrone.vy = %3.2f [m/s]\n", vy);
        //printf("ardrone.vz = %3.2f [m/s]\n", vz);

        // Battery
        int battery = ardrone.getBatteryPercentage();
        printf("ardrone.battery = %d [%%]\n", battery);

        // Take off / Landing
        if (KEY_PUSH(VK_CONTROL)) {
            if (ardrone.onGround()) ardrone.takeoff();
            else                    ardrone.landing();
        }

        // Emergency stop
        if (KEY_PUSH(VK_RETURN)) ardrone.emergency();

        if (KEY_PUSH(VK_TAB)) hand_control=!hand_control;

        // AR.Drone is flying
        if (!ardrone.onGround()) {
            // Move
            double x = 0.0, y = 0.0, z = 0.0, r = 0.0;
            if (KEY_DOWN(VK_UP))    x =  0.5;
            if (KEY_DOWN(VK_DOWN))  x = -0.5;
            if (KEY_DOWN(VK_LEFT))  y=  0.5;
            if (KEY_DOWN(VK_RIGHT)) y = -0.5;
            if (KEY_DOWN('Q'))      z =  0.5;
            if (KEY_DOWN('A'))      z = -0.5;
            if (KEY_DOWN('O'))      r =  0.5;
            if (KEY_DOWN('P'))      r = -0.5;
            ardrone.move3D(x, y, z, r);
        }

        // Change camera
        static int mode = 0;
        if (KEY_PUSH('C')) ardrone.setCamera(++mode%4);

        // Display the image
        cvShowImage("camera", image);



       // cvWaitKey(1);
    }

    // See you
    ardrone.close();

    return 0;
}