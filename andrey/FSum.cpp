

#include <iostream>
#include <math.h>
#include <cmath>
#include <stdio.h>
#include <limits>
#include <fstream>

using namespace std;

double K =1.38e-23;
double ge = 2.0;
double gi = 0.5;
double ga = 1.0;
double me = 9.1e-31;
double h = 6.62e-34;
double e = -1.6e-19;
double eps0 = 8.85e-12;
// x[i], y[i]
double x1 = 5.0;
double yy1 = 6.0;
double x2 = 4.0;
double y2 = 2.0;
double I;
double np;
double nA;
double dt;

double R = 0;
double Fk = 0;
double Z = 0;
double E=0;
double FSum;
double Fdd;
double T=0;
double rp=0;
double rD=0;
double fi=0;
double V=0;
double ny=0;

ifstream in3;
ofstream out3;

double Rresearch(double xx1, double xx2, double yyy1, double yy2)
{
	R = sqrt((xx1-xx2)*(xx1-xx2)+(yyy1-yy2)*(yyy1-yy2));
	return R;
}

double Eresearch()
{
	double Rr = Rresearch(x1,x2,yy1,y2);
	E=(-2*K*T*(rp/Rr) * (1/rD+1/Rr))/e*sinh(log(tanh(e*fi)))/((4*K*T)+copysign(1,fi)*(rp-Rr)/rD);
	return E;
}

double FkResearch()
{
	Fk = e*Z*Eresearch();
	return Fk;
}

double FSumResearch()
{
	double Fkk = FkResearch();
	FSum = Fdd - Fkk;
	cout<<"FSum = "<<FSum<<endl;
	return FSum;
}


int main() {

	FSumResearch();

	out3.open("out3.txt",ios::out);
	out3<<FSum<<endl;


}




