#include <iostream>
#include <math.h>
#include <cmath>
#include <stdio.h>
#include <limits>
#include <fstream>

using namespace std;

	const double nA = 2e+23;
	double Cc = 35; // C для проверочного построения
	const double n0 = 4.0e+16;
	double rD = 4.0e-7;
	const double rp = 1.0e-8;
	double rpi = rp;
	double rpj = rp;
	const double np = 1.0e+18;
	const double lambdaR = 0.5e-6;
	const double K = 1.3806488e-23;
	const double e = 1.6021176565e-19;

	const double m=1.0;
	const double D=1.0;

	// входные данные из 1-й задачи
	double T = 2300; // Т для проверочного построения
	double I;
	double w;
	double ne;
	//double np; определены выше
	//double rp; определены выше
	double Z=1;
	double  R[7] = {100000,500000,1000000,5000000,10000000,50000000,100000000};// [10^5, 10^8]
	// double nA; определена выше
	double Fis = 1.0;
	double Fisj = 1.0; // =Fis
	double Fisi = 1.0; // =Fis

	// считаем и заносим результаты сюда
	double VT;
	double tauR;
	double C;
	double Fdd[7];

	ifstream in;
	ofstream out;


	double VTresearch()
	{
		VT = sqrt((8*K*T)/(M_PI*m));
		cout<<"VT = "<<VT<<endl;

		return VT;
	}

	double TauRresearch()
	{
		tauR = lambdaR/VTresearch();
		cout<<"tauR = "<<tauR<<endl;

		return tauR;
	}

	double Cresearch()
	{
		//C = (4*M_PI*rp*rp*n0*lambdaR*D)/(rd*tauR);
		cout<<"C = "<<Cc<<endl;

		return Cc;//при финальных расчётах ввозвращаем С а не Сс
	}

	double rdResearch()
	{
		// nq = sqrt(ne*ne - Z*np);
		rD = sqrt( (K*T)/(8*M_PI*e*e* (sqrt(ne*ne - Z*np))) );
		cout<<"rD = "<<rD<<endl;

		return rD;
	}

	void FddResearch()
	{
		for (int i = 0; i < 7; i++)
		{
			Fdd[i] = C*rpi*rpi*((exp(e*Fisi/K*T)-1)/(sqrt(2*cosh( e*Fisi/K*T)) )) * exp( rpj/R[i]*tanh(e*Fisj/4*K*T) );
			cout<<R[i]<<" - "<<Fdd[i]<<endl;
		}


	}

	// функция записи в файл



int main() {

	// VTresearch();
	// TauRresearch();
	// Cresearch();
	// rdResearch();

	FddResearch();

	out.open("out.txt",ios::out);
	for (int i = 0; i < 7; i++)
	{
		out<<T<<" "<<I<<" "<<w<<" "<<np<<" "<<rp<<" "<<nA<<" "<<ne<<" "<<Z<<" "<<rD<<" "<<Fis<<" "<<R[i]<<" "<<Fdd[i]<<endl;

	}

	 return 0;

}




