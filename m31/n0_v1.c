#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//parametry plazmi

#define NA 1e20
#define I 3.893
#define I2 4.34
#define I3 5.1
#define r 1e-6
#define np 1e15
#define K 1.38e-23
#define T 2500.0
#define ge 2.0
#define gi 0.5
#define ga 1.0
#define me 9.1e-31
#define h 6.62e-34
#define E0 8.85e-12
#define e -1.6e-19
#define rp 1e-6

int main(int argc, char* argv[])
{

// raschet konstanl
double ks =  (ge * gi / ga ) * pow((( 2.0 * M_PI * me * K * T) / (h * h)), 1.5) ;
double b= ks * exp( - ( I * 1.6e-19 ) / K / T ) ;
double bNA = b*NA;
double n0 = (- b + sqrt( b * b + 4.0 * bNA ) ) / 2.0;

FILE *file;
if (!(file = fopen("out.cvs","w")))
	{
		printf("А файлик-то и не открылси!"); abort();
	}

fprintf(file, "n0 T = 2000 NA 1e20 I = 3.89 \n");
fprintf(file, "%.2e; \n", n0);



fprintf(file, "n0(NA) NA = 1e16:1e21 T=const (2500) I=5.1 I=4.34 I=3.89 \n");
double NA1 = 1.0e16;
do {
  NA1 = NA1*1.5;
	
double ks1 =  (ge * gi / ga ) * pow((( 2.0 * M_PI * me * K * T) / (h * h)), 1.5) ;
double b1 = ks1 * exp( - ( I * 1.6e-19 ) / K / T ) ;
double bNA1 = b1*NA1;
double n01 = (- b1 + sqrt( b1 * b1 + 4.0 * bNA1 ) ) / 2.0;

double ks2 =  (ge * gi / ga ) * pow((( 2.0 * M_PI * me * K * T) / (h * h)), 1.5) ;
double b2 = ks2 * exp( - ( I2 * 1.6e-19 ) / K / T ) ;
double bNA2 = b2*NA1;
double n02 = (- b2 + sqrt( b2 * b2 + 4.0 * bNA2 ) ) / 2.0;

double ks3 =  (ge * gi / ga ) * pow((( 2.0 * M_PI * me * K * T) / (h * h)), 1.5) ;
double b3 = ks3 * exp( - ( I3 * 1.6e-19 ) / K / T ) ;
double bNA3 = b3*NA1;
double n03 = (- b3 + sqrt( b3 * b3 + 4.0 * bNA3 ) ) / 2.0;

fprintf(file, "%.2e; %.2e; %.2e; %.2e;\n", NA1, n01, n02, n03);
	}
	while ( NA1 < 1.0e21);

fprintf(file, "n0(T) T = 2000:3000 NA=const \n");

double T1 = 2000;
do {
  T1 = T1+50;
	
double ks4 =  (ge * gi / ga ) * pow((( 2.0 * M_PI * me * K * T1) / (h * h)), 1.5) ;
double b4 = ks4 * exp( - ( I * 1.6e-19 ) / K / T1 ) ;
double bNA4 = b4*NA;
double n04 = (- b4 + sqrt( b4 * b4 + 4.0 * bNA4 ) ) / 2.0;

double ks5 =  (ge * gi / ga ) * pow((( 2.0 * M_PI * me * K * T1) / (h * h)), 1.5) ;
double b5 = ks5 * exp( - ( I2 * 1.6e-19 ) / K / T1 ) ;
double bNA5 = b5*NA;
double n05 = (- b5 + sqrt( b5 * b5 + 4.0 * bNA5 ) ) / 2.0;

double ks6 =  (ge * gi / ga ) * pow((( 2.0 * M_PI * me * K * T1) / (h * h)), 1.5) ;
double b6 = ks6 * exp( - ( I3 * 1.6e-19 ) / K / T1 ) ;
double bNA6 = b6*NA;
double n06 = (- b6 + sqrt( b6 * b6 + 4.0 * bNA6 ) ) / 2.0;

fprintf(file, "%.2e; %.2e; %.2e; %.2e;\n", T1, n04, n05, n06);
	}
	while ( T1 < 3000.0);


fclose(file);

}
